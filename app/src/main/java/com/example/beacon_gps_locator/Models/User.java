package com.example.beacon_gps_locator.Models;

public class User {

    String fullName, email, country, fullAddress, bikeBrand, bikeType, phone, imgUrl, token;

    public User(String fullname, String email, String country, String fullAddress, String bicycleBrand, String bicycleType, String phone, String imgUrl, String token) {
        this.fullName = fullname;
        this.email = email;
        this.country = country;
        this.fullAddress = fullAddress;
        this.bikeBrand = bicycleBrand;
        this.bikeType = bicycleType;
        this.phone = phone;
        this.imgUrl = imgUrl;
    }

    public User() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getBikeBrand() {
        return bikeBrand;
    }

    public void setBikeBrand(String bikeBrand) {
        this.bikeBrand = bikeBrand;
    }

    public String getBikeType() {
        return bikeType;
    }

    public void setBikeType(String bikeType) {
        this.bikeType = bikeType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
