package com.example.beacon_gps_locator.Models;

public class CycleSummary {
    String date, earned, distance, time_start, time_end, points, day;

    public CycleSummary(String date, String earned, String distance, String time_start,String time_end, String points, String day) {
        this.date = date;
        this.earned = earned;
        this.distance = distance;
        this.time_start = time_start;
        this.points = points;
        this.time_end = time_end;
        this.day = day;
    }

    public CycleSummary() {

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEarned() {
        return earned;
    }

    public void setEarned(String earned) {
        this.earned = earned;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime_start() {
        return time_start;
    }

    public void setTime_start(String time_start) {
        this.time_start = time_start;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getTime_end() {
        return time_end;
    }

    public void setTime_end(String time_end) {
        this.time_end = time_end;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
