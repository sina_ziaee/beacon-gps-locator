package com.example.beacon_gps_locator.Models;

import java.util.ArrayList;

public class Campaign {
    private String companyName;
    private String imgURL;
    private String fromDate;
    private String toDate;
    private ArrayList<String> users_joined_campaign;

    public Campaign() {
    }

    public Campaign(String companyName, String imgURL, ArrayList<String> users_joined_campaign, String fromDate, String toDate) {
        this.companyName = companyName;
        this.imgURL = imgURL;
        this.users_joined_campaign = users_joined_campaign;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public ArrayList<String> getUsers_joined_campaign() {
        return users_joined_campaign;
    }

    public void setUsers_joined_campaign(ArrayList<String> users_joined_campaign) {
        this.users_joined_campaign = users_joined_campaign;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
