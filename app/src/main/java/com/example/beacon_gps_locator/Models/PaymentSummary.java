package com.example.beacon_gps_locator.Models;

public class PaymentSummary {
    String date, status, amount;

    public PaymentSummary(String date, String status, String amount) {
        this.date = date;
        this.status = status;
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
