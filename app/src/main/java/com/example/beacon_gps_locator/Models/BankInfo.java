package com.example.beacon_gps_locator.Models;

public class BankInfo {
    String bankName, bankIBANNumber, accountName, imgUrl;

    public BankInfo(String bankName, String bankIBANNumber, String accountName, String imgUrl) {
        this.bankName = bankName;
        this.bankIBANNumber = bankIBANNumber;
        this.accountName = accountName;
        this.imgUrl = imgUrl;
    }

    public BankInfo() {

    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankIBANNumber() {
        return bankIBANNumber;
    }

    public void setBankIBANNumber(String bankIBANNumber) {
        this.bankIBANNumber = bankIBANNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
