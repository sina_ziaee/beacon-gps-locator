package com.example.beacon_gps_locator.Models;

public class Message {

    String messageBody, sender, receiver, date, time;

    public Message() {
    }

    public Message(String messageBody, String sender, String receiver, String date, String time) {
        this.messageBody = messageBody;
        this.sender = sender;
        this.receiver = receiver;
        this.date = date;
        this.time = time;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
