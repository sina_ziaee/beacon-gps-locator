package com.example.beacon_gps_locator.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.beacon_gps_locator.Activities.BankActivity;
import com.example.beacon_gps_locator.Activities.CustomerSupportActivity;
import com.example.beacon_gps_locator.Activities.EditProfileActivity;
import com.example.beacon_gps_locator.Activities.LogOnActivity;
import com.example.beacon_gps_locator.Activities.SafetyActivity;
import com.example.beacon_gps_locator.Activities.SettingsActivity;
import com.example.beacon_gps_locator.Models.BankInfo;
import com.example.beacon_gps_locator.Models.User;
import com.example.beacon_gps_locator.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ProfileFragment extends Fragment {

    Button btn_edit_profile;
    RelativeLayout rel_bank;
    LinearLayout rel_safety, rel_settings, rel_customer_support;
    String currentUser = "";
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users");
    DatabaseReference bankRef = FirebaseDatabase.getInstance().getReference().child("BankInfo");

    ImageView imgProfilePic;
    TextView tv_name, tv_phone, tv_accountNo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        initFields(view);

        setListeners();

        retrieveInfo();

        return view;
    }

    private void initFields(View view) {
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(getContext(), LogOnActivity.class));
            ((Activity) getContext()).finish();
        }
        currentUser = mAuth.getCurrentUser().getUid();
        userRef = userRef.child(currentUser);
        bankRef = bankRef.child(currentUser);

        tv_name = view.findViewById(R.id.tv_profile_name);
        tv_phone = view.findViewById(R.id.tv_phone_profile);
        imgProfilePic = view.findViewById(R.id.img_profile_pic);
        btn_edit_profile = view.findViewById(R.id.btn_edit_profile);
        rel_bank = view.findViewById(R.id.rel_bank);
        rel_customer_support = view.findViewById(R.id.rel_customer_support);
        rel_safety = view.findViewById(R.id.rel_safety);
        rel_settings = view.findViewById(R.id.rel_Settings);
        tv_accountNo = view.findViewById(R.id.tv_account_name);
    }

    private void setListeners() {
        btn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoEditProfileActivity();
            }
        });
        rel_bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoBankActivity();
            }
        });
        rel_safety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoSafetyActivity();
            }
        });

        rel_customer_support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoCustomerSupportActivity();
            }
        });
        rel_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoSettingsActivity();
            }
        });
    }

    private void gotoEditProfileActivity() {
        startActivity(new Intent(getContext(), EditProfileActivity.class));
    }

    private void gotoBankActivity(){
        startActivity(new Intent(getContext(), BankActivity.class));
    }

    private void gotoSettingsActivity(){
        startActivity(new Intent(getContext(), SettingsActivity.class));
    }

    private void gotoCustomerSupportActivity(){
        startActivity(new Intent(getContext(), CustomerSupportActivity.class));
    }

    private void gotoSafetyActivity(){
        startActivity(new Intent(getContext(), SafetyActivity.class));
    }

    public static ProfileFragment newInstance() {

        Bundle args = new Bundle();

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void retrieveInfo(){
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    User user = snapshot.getValue(User.class);
                    String name = user.getFullName();
                    String phone = user.getPhone();
                    String imgUrl = user.getImgUrl();

                    tv_name.setText(name);
                    tv_phone.setText(phone);
                    if (imgUrl.length() != 0){
                        Picasso.get().load(imgUrl).into(imgProfilePic);
                    }

                    bankRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                BankInfo  bankInfo = snapshot.getValue(BankInfo.class);
                                String accountName = bankInfo.getAccountName();
                                tv_accountNo.setText(accountName);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}
