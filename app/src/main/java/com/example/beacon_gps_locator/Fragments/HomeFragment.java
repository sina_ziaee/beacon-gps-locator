package com.example.beacon_gps_locator.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_gps_locator.Activities.LogOnActivity;
import com.example.beacon_gps_locator.Adapters.CampaignAdapter;
import com.example.beacon_gps_locator.Models.Campaign;
import com.example.beacon_gps_locator.Models.CycleSummary;
import com.example.beacon_gps_locator.R;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class HomeFragment extends Fragment {

    RecyclerView recyclerView;
    static Context mContext;
    CampaignAdapter adapter;
    TextView tv_see_all;
    TextView tv_journeys;
    TextView tv_earned;
    TextView tv_total_distance_cycled;


    //firebase Stuff
    DatabaseReference campaignRef = FirebaseDatabase.getInstance().getReference().child("Campaigns");
    DatabaseReference cycleRef = FirebaseDatabase.getInstance().getReference().child("CyclesHistory");
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    String currentUser = "";

    public static HomeFragment newInstance(Context context) {
        Bundle args = new Bundle();

        mContext = context;

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initFields(view);

        setListeners();

        retrieveInfo();

        setCampaignRecyclerView();

        return view;
    }

    private void setCampaignRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        FirebaseRecyclerOptions<Campaign> options = new FirebaseRecyclerOptions.Builder<Campaign>()
                .setQuery(campaignRef, Campaign.class)
                .build();

        adapter = new CampaignAdapter(options);
        recyclerView.setAdapter(adapter);
    }

    private void setListeners() {

    }

    private void initFields(View view) {
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(mContext, LogOnActivity.class));
            ((Activity) mContext).finish();
        }
        currentUser = mAuth.getCurrentUser().getUid();
        cycleRef = cycleRef.child(currentUser);
        recyclerView = view.findViewById(R.id.rec_campaigns_in_home_page);
        tv_journeys = view.findViewById(R.id.total_journeys);
        tv_earned = view.findViewById(R.id.total_earned);
        tv_total_distance_cycled = view.findViewById(R.id.total_cycled);
        tv_see_all = view.findViewById(R.id.tv_see_all);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.startListening();
    }

    @Override
    public void onPause() {
        super.onPause();
        adapter.stopListening();
    }

    private void retrieveInfo(){
        cycleRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean flag = false;
                if (snapshot.exists()){
                    int counter = 0;
                    float distance = 0;
                    for (DataSnapshot ds: snapshot.getChildren()){
                        counter++;
                        CycleSummary cycleSummary = ds.getValue(CycleSummary.class);
                        if (cycleSummary.getDistance() != null && cycleSummary.getDistance().length() != 0){
                            flag = true;
                            String distanceTemp = cycleSummary.getDistance();
                            distance += Float.parseFloat(distanceTemp);
                        }
                    }
                    if (flag == true){
                        distance = distance/1000;
                        String num = String.valueOf(distance);
                        int inx = num.indexOf(".");
                        tv_journeys.setText(counter+"");
                        tv_total_distance_cycled.setText(num.substring(0, inx+3));
                        Log.v("Tagger", num.substring(0, inx+2));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}
