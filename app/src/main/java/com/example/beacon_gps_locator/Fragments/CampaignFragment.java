package com.example.beacon_gps_locator.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_gps_locator.Adapters.CampaignAdapter;
import com.example.beacon_gps_locator.Models.Campaign;
import com.example.beacon_gps_locator.R;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CampaignFragment extends Fragment {

    RecyclerView recyclerView;
    static Context mContext;
    CampaignAdapter adapter;

    //firebase Stuff
    DatabaseReference campaignRef = FirebaseDatabase.getInstance().getReference().child("Campaigns");

    public static CampaignFragment newInstance() {

        Bundle args = new Bundle();

        CampaignFragment fragment = new CampaignFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_campaign, container, false);

        initFields(view);

        setListeners();

        setCampaignRecyclerView();

        return view;
    }

    private void initFields(View view) {
        recyclerView = view.findViewById(R.id.rec_campaigns);
    }

    private void setListeners() {

    }

    private void setCampaignRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        FirebaseRecyclerOptions<Campaign> options = new FirebaseRecyclerOptions.Builder<Campaign>()
                .setQuery(campaignRef, Campaign.class)
                .build();

        adapter = new CampaignAdapter(options);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.startListening();
    }

    @Override
    public void onPause() {
        super.onPause();
        adapter.stopListening();
    }

}
