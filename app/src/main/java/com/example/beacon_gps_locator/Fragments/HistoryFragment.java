package com.example.beacon_gps_locator.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_gps_locator.Activities.LogOnActivity;
import com.example.beacon_gps_locator.Adapters.CycleAdapter;
import com.example.beacon_gps_locator.Adapters.PaymentAdapter;
import com.example.beacon_gps_locator.Models.CycleSummary;
import com.example.beacon_gps_locator.Models.PaymentSummary;
import com.example.beacon_gps_locator.R;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HistoryFragment<ActivityEachApp> extends Fragment {

    RelativeLayout relativeLayout, rel_due;
    Button btn_cycle_history, btn_payment_history;
    boolean isInCycleHistory = true;
    RecyclerView recyclerView;

    CycleAdapter cycleAdapter;
    PaymentAdapter paymentAdapter;


    static Context mContext;

    int colorBlack;
    int colorWhite;
    int colorBlue;

    //firebase stuff
    DatabaseReference paymentRef = FirebaseDatabase.getInstance().getReference().child("PaymentsHistory");
    DatabaseReference cycleRef = FirebaseDatabase.getInstance().getReference().child("CyclesHistory");
    FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public static HistoryFragment newInstance(Context context) {

        mContext = context;

        Bundle args = new Bundle();
//        args.getParcelable("context");
        HistoryFragment fragment = new HistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        colorBlack = ContextCompat.getColor(getContext(), R.color.black);
        colorWhite = ContextCompat.getColor(getContext(), R.color.white);
        colorBlue = ContextCompat.getColor(getContext(), R.color.blue_light);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        initFields(view);

        setListeners();

        loadCycleHistory();

        return view;
    }

    private void initFields(View view) {
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(mContext, LogOnActivity.class));
            ((Activity) mContext).finish();
        }
        String currentUser = mAuth.getCurrentUser().getUid();
        paymentRef = paymentRef.child(currentUser);
        cycleRef = cycleRef.child(currentUser);
        /////////////////////////////////////////
        relativeLayout = view.findViewById(R.id.rel);
        rel_due = view.findViewById(R.id.temp2);
        btn_cycle_history = view.findViewById(R.id.btn_cycle_trip);
        btn_payment_history = view.findViewById(R.id.btn_payments);
        recyclerView = view.findViewById(R.id.rec_history);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        FirebaseRecyclerOptions<PaymentSummary> options = new FirebaseRecyclerOptions.Builder<PaymentSummary>()
                .setQuery(paymentRef, PaymentSummary.class)
                .build();

        paymentAdapter = new PaymentAdapter(options);

        FirebaseRecyclerOptions<CycleSummary> options2 = new FirebaseRecyclerOptions.Builder<CycleSummary>()
                .setQuery(cycleRef, CycleSummary.class)
                .build();

        cycleAdapter = new CycleAdapter(options2);

    }

    private void setListeners() {
        btn_payment_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInCycleHistory == true){
                    // first change the color
                    rel_due.setVisibility(View.VISIBLE);
                    btn_payment_history.setTextColor(colorWhite);
                    btn_payment_history.setBackgroundResource(R.drawable.btn_back_blue);
                    btn_cycle_history.setTextColor(colorBlack);
                    btn_cycle_history.setBackgroundResource(R.drawable.btn_back_white);
                    // then change the boolean
                    isInCycleHistory = false;
                    // then load payments history
                    loadPaymentsHistory();
                }
                else {
                    // do nothing
                }
            }
        });
        btn_cycle_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInCycleHistory == true){
                    // do nothing
                }
                else {
                    // first change the color of button
                    rel_due.setVisibility(View.INVISIBLE);
                    btn_payment_history.setTextColor(colorBlack);
                    btn_payment_history.setBackgroundResource(R.drawable.btn_back_white);
                    btn_cycle_history.setTextColor(colorWhite);
                    btn_cycle_history.setBackgroundResource(R.drawable.btn_back_blue);
                    // then change the boolean
                    isInCycleHistory = true;
                    // then load cycle trips history
                    loadCycleHistory();
                }
            }
        });
    }

    private void loadCycleHistory() {
        paymentAdapter.stopListening();
        cycleAdapter.startListening();
//        FirebaseRecyclerOptions<CycleSummary> options = new FirebaseRecyclerOptions.Builder<CycleSummary>()
//                .setQuery(cycleRef, CycleSummary.class)
//                .build();
//
//        cycleAdapter = new CycleAdapter(options);
        recyclerView.setAdapter(cycleAdapter);
    }

    private void loadPaymentsHistory() {

        cycleAdapter.stopListening();
        paymentAdapter.startListening();

//        FirebaseRecyclerOptions<PaymentSummary> options = new FirebaseRecyclerOptions.Builder<PaymentSummary>()
//                .setQuery(paymentRef, PaymentSummary.class)
//                .build();
//
//        paymentAdapter = new PaymentAdapter(options);
        recyclerView.setAdapter(paymentAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isInCycleHistory == true){
            cycleAdapter.startListening();
        }
        else {
            paymentAdapter.startListening();
        }
//        cycleAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (isInCycleHistory == true){
            cycleAdapter.stopListening();
        }
        else {
            paymentAdapter.stopListening();
        }
//        cycleAdapter.stopListening();
    }
}
