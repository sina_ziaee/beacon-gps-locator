package com.example.beacon_gps_locator.BackgroundWorks;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.example.beacon_gps_locator.Activities.MainActivity;
import com.example.beacon_gps_locator.R;

import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;

public class BeaconReferenceApplication extends Application implements BootstrapNotifier {

    private static final String TAG = "Tagger";
    private RegionBootstrap regionBootstrap;
    //    private BackgroundPowerSaver backgroundPowerSaver;
    private boolean haveDetectedBeaconsSinceBoot = false;
    private BeaconManager beaconManager;
    private String message;
    private MainActivity mainActivity;

    @Override
    public void onCreate() {
        super.onCreate();

        setBeaconManager();

    }

    private void setBeaconManager()
    {
        beaconManager = org.altbeacon.beacon.BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));

        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:0-3=4c000215,i:4-19,i:20-21,i:22-23,p:24-24"));

        beaconManager.setDebug(true);
        message = "Promotion is running";
        logger("setting up background monitoring for beacons and power saving");

        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.image);
        builder.setContentTitle(message);
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT
        );

        builder.setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "1234",
                    "Beacon Locator",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Looking for beacons");
            NotificationManager notificationManager = (NotificationManager) getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channel.getId());
        }

        beaconManager.enableForegroundServiceScanning(builder.build(), 456);
        beaconManager.setEnableScheduledScanJobs(false);
        beaconManager.setBackgroundBetweenScanPeriod(5000);
        beaconManager.setBackgroundScanPeriod(1100);

        Region region = new Region("backgroundRegion",
                null, null, null);
        regionBootstrap = new RegionBootstrap(this, region);

    }

    public void disableMonitoring() {
        if (regionBootstrap != null) {
            regionBootstrap.disable();
            regionBootstrap = null;
        }
    }
    public void enableMonitoring() {
        Region region = new Region("backgroundRegion",
                null, null, null);
        regionBootstrap = new RegionBootstrap(this, region);
    }

    @Override
    public void didEnterRegion(Region region) {
        message = "Did enter a region.";
        logger(message);

        if (!haveDetectedBeaconsSinceBoot) {
            Log.v(TAG, "auto launching MainActivity");

            haveDetectedBeaconsSinceBoot = true;
        }
        else {
            message= "A beacon Nearby";
            if (mainActivity != null) {
                // If the Monitoring Activity is visible, we log info about the beacons we have
                // seen on its display
                logger("I see a beacon again" );
            }
            else {
                // If we have already seen beacons before, but the monitoring activity is not in
                // the foreground, we send a notification to the user on subsequent detections.
                logger(message);
                sendNotification(message);
            }
        }


    }

    @Override
    public void didExitRegion(Region region) {
        logger("I no longer see a beacon.");
        sendNotification("I no longer see your beacon.");
        MainActivity.isGettingBeaconSignal = false;
    }

    @Override
    public void didDetermineStateForRegion(int state, Region region) {
        logger("Current region state is: " + (state == 1 ? "INSIDE" : "OUTSIDE ("+state+")"));
    }

    private void sendNotification(String notification) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this, "1234")
                        .setContentTitle("Beacon Reference Application")
                        .setContentText(notification)
                        .setSmallIcon(R.drawable.image);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(new Intent(this, MainActivity.class));
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }

    private void logger(String line){
        Log.v(TAG, line);
    }

}
