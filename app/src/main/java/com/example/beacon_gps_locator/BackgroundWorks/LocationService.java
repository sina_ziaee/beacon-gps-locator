package com.example.beacon_gps_locator.BackgroundWorks;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.beacon_gps_locator.Activities.MainActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class LocationService extends Service {

    public static FusedLocationProviderClient fusedLocationProviderClient;
    public static LocationCallback locationCallback;
    public static boolean isRunning = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location location = locationResult.getLastLocation();
                float accuracy = location.getAccuracy();
                Log.v("Tagger", accuracy + "");

                if (accuracy < 20){
                    double lat = location.getLatitude();
                    double lng = location.getLongitude();

                    Intent intent = new Intent("ACT_LOC");
                    intent.putExtra("latitude", lat);
                    intent.putExtra("longitude", lng);
                    sendBroadcast(intent);
                }
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @SuppressLint("MissingPermission")
    private void requestLocation() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    // tells us that the service is started
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (isRunning == false){
            requestLocation();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public static void stopLocationService(){
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

}
