package com.example.beacon_gps_locator.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_gps_locator.Activities.MainActivity;
import com.example.beacon_gps_locator.Activities.UtilsActivity;
import com.example.beacon_gps_locator.R;

import org.altbeacon.beacon.Beacon;

import java.util.ArrayList;

public class BeaconsAdapter extends RecyclerView.Adapter<BeaconsAdapter.BeaconHolder> {

    ArrayList<Beacon> beacons_list;
    Context mContext;

    public BeaconsAdapter(ArrayList<Beacon> beacons_list) {
        this.beacons_list = beacons_list;
    }

    @NonNull
    @Override
    public BeaconHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.beacon_item, parent, false);
        return new BeaconHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BeaconHolder holder, int position) {
        Beacon beacon = beacons_list.get(position);
        if (beacon.getId1() != null){
            String id1 = beacon.getId1().toString();
            holder.tv_id_1.append(id1);
        }
        if (beacon.getId1() != null){
            String id2 = beacon.getId2().toString();
            holder.tv_id_2.append(id2);
        }
        if (beacon.getId1() != null){
            String id3 = beacon.getBluetoothAddress();
            holder.tv_id_3.append(id3);
        }

        if (beacon.getId1() == null){
            String temp = beacon.toString();
            holder.tv_id_1.setText(temp);
        }

    }

    @Override
    public int getItemCount() {
        return beacons_list.size();
    }

    class BeaconHolder extends RecyclerView.ViewHolder{

        TextView tv_id_1;
        TextView tv_id_2;
        TextView tv_id_3;
        RelativeLayout relativeLayout;

        public BeaconHolder(@NonNull View itemView) {
            super(itemView);
            tv_id_1 = itemView.findViewById(R.id.tv_id_1);
            tv_id_2 = itemView.findViewById(R.id.tv_id_2);
            tv_id_3 = itemView.findViewById(R.id.tv_id_3);
            mContext = itemView.getContext();
            relativeLayout = itemView.findViewById(R.id.rel_beacon);
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    UtilsActivity.beacon_id1 = beacons_list.get(position).getId1().toString();
                    gotoMainActivity();
                }
            });
        }

        public void gotoMainActivity(){
            MainActivity.isGettingBeaconSignal = true;
            Intent intent = new Intent();
            intent.putExtra("is_selected_beacon", true);

//            Intent intent = new Intent(mContext, MainActivity.class);
//            intent.putExtra("is_selected_beacon", true);
//            mContext.startActivity(intent);
            ((Activity) mContext).setResult(Activity.RESULT_OK, intent);
            ((Activity) mContext).finish();
        }

    }

}
