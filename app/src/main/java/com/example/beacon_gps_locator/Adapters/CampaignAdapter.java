package com.example.beacon_gps_locator.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_gps_locator.Models.Campaign;
import com.example.beacon_gps_locator.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CampaignAdapter extends FirebaseRecyclerAdapter<Campaign, CampaignAdapter.CampaignViewHolder> {

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public CampaignAdapter(@NonNull FirebaseRecyclerOptions<Campaign> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull CampaignViewHolder holder, int position, @NonNull Campaign model) {
        ArrayList<String> list = model.getUsers_joined_campaign();
        String companyName = model.getCompanyName();
        String companyImgURL = model.getImgURL();
        String fromDate = model.getFromDate();
        String toDate = model.getToDate();
        String fromToDate = fromDate + " till " + toDate;
        String currentUser = mAuth.getCurrentUser().getUid();

        Picasso.get().load(companyImgURL).placeholder(R.drawable.btn_back_blue).into(holder.img_company);
        holder.tv_company_name.setText(companyName);
        holder.tv_from_to_date.setText(fromToDate);

        if (list.contains(currentUser)){
            holder.btn_apply_campaign.setText("Joined");
        }
        else {
            holder.btn_apply_campaign.setText("Apply");
        }
    }

    @NonNull
    @Override
    public CampaignViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.camaign_item, parent, false);
        return new CampaignViewHolder(view);
    }

    class CampaignViewHolder extends RecyclerView.ViewHolder{

        TextView tv_company_name, tv_from_to_date;
        ImageView img_company;
        Button btn_apply_campaign;

        public CampaignViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_company_name = itemView.findViewById(R.id.tv_company_name);
            tv_from_to_date = itemView.findViewById(R.id.campaign_date);
            img_company = itemView.findViewById(R.id.img_company);
            btn_apply_campaign = itemView.findViewById(R.id.btn_apply_campaign);
            btn_apply_campaign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }

    }

}
