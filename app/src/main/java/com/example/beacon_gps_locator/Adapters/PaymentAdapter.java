package com.example.beacon_gps_locator.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_gps_locator.Models.PaymentSummary;
import com.example.beacon_gps_locator.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

public class PaymentAdapter extends FirebaseRecyclerAdapter<PaymentSummary, PaymentAdapter.PaymentHolder> {

    public PaymentAdapter(@NonNull FirebaseRecyclerOptions<PaymentSummary> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull PaymentHolder holder, int position, @NonNull PaymentSummary model) {
        String amount = model.getAmount();
        String date = model.getDate();
        String status = model.getStatus();
    }

    @NonNull
    @Override
    public PaymentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_history_item, parent, false);
        return new PaymentHolder(view);
    }

    class PaymentHolder extends RecyclerView.ViewHolder{

        TextView tv_day, tv_date, tv_status, tv_amount;

        public PaymentHolder(@NonNull View itemView) {
            super(itemView);
            tv_day = itemView.findViewById(R.id.tv_day_payment);
            tv_amount = itemView.findViewById(R.id.tv_amount_payment);
            tv_status = itemView.findViewById(R.id.tv_status_payment);
            tv_date = itemView.findViewById(R.id.tv_date_payment);
        }
    }

}
