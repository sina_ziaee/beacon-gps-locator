package com.example.beacon_gps_locator.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_gps_locator.Models.Message;
import com.example.beacon_gps_locator.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;

public class MessageAdapter extends FirebaseRecyclerAdapter<Message, MessageAdapter.MessageViewHolder> {

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public MessageAdapter(@NonNull FirebaseRecyclerOptions<Message> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull MessageViewHolder holder, int position, @NonNull Message model) {
        final int tempPosition = position;

        String msgBody = model.getMessageBody();
        String sender = model.getSender();
        String receiver = model.getReceiver();
        String time = model.getTime();
        String date = model.getDate();

        if (sender.equals(mAuth.getCurrentUser().getUid())){
            holder.tv_company.setVisibility(View.GONE);
            holder.tv_user.setVisibility(View.VISIBLE);
            holder.tv_user.setText(msgBody);
            holder.tv_time_user.setVisibility(View.VISIBLE);
            holder.tv_time_company.setVisibility(View.GONE);
            holder.tv_time_user.setText(time);
        }
        else {
            holder.tv_company.setVisibility(View.VISIBLE);
            holder.tv_user.setVisibility(View.GONE);
            holder.tv_company.setText(msgBody);
            holder.tv_time_user.setVisibility(View.GONE);
            holder.tv_time_company.setVisibility(View.VISIBLE);
            holder.tv_time_company.setText(time);
        }

    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item, parent, false);
        return new MessageViewHolder(view);
    }

    class MessageViewHolder extends RecyclerView.ViewHolder{

        TextView tv_company, tv_user, tv_time_company, tv_time_user;

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_company = itemView.findViewById(R.id.tv_company);
            tv_user = itemView.findViewById(R.id.tv_user);
            tv_time_user = itemView.findViewById(R.id.tv_time_user);
            tv_time_company = itemView.findViewById(R.id.tv_time_company);
        }
    }

}
