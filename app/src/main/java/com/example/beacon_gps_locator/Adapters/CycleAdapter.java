package com.example.beacon_gps_locator.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_gps_locator.Activities.MapActivity;
import com.example.beacon_gps_locator.Models.CycleSummary;
import com.example.beacon_gps_locator.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CycleAdapter extends FirebaseRecyclerAdapter<CycleSummary, CycleAdapter.CycleHolder> {

    public CycleAdapter(@NonNull FirebaseRecyclerOptions<CycleSummary> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull CycleHolder holder, int position, @NonNull CycleSummary model) {
        String date = model.getDate();
        String distance = model.getDistance();
        String earned = model.getEarned();
        String points = model.getPoints();
        String time = model.getTime_start();
        if (model.getDay() != null){
            String day = model.getDay();
            holder.day.setText(day);
        }
        holder.date.setText(date);
        holder.summary.setText(distance);

    }

    @NonNull
    @Override
    public CycleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cycle_history_item, parent, false);
        return new CycleHolder(view);
    }

    class CycleHolder extends RecyclerView.ViewHolder{

        Context mContext;
        public TextView date, day, summary, earned;
        public CycleHolder(@NonNull View itemView) {
            super(itemView);
            day = itemView.findViewById(R.id.tv_day);
            date = itemView.findViewById(R.id.tv_date_map);
            summary = itemView.findViewById(R.id.tv_cycle_summary);
            earned = itemView.findViewById(R.id.tv_cycle_earned);
            mContext = itemView.getContext();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(mContext, MapActivity.class);
                    String date = getItem(position).getDate();
                    intent.putExtra("date", date);
                    mContext.startActivity(intent);
                }
            });
        }
    }

}
