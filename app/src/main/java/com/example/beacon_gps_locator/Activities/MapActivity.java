package com.example.beacon_gps_locator.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beacon_gps_locator.BackgroundWorks.BeaconReferenceApplication;
import com.example.beacon_gps_locator.Models.CycleSummary;
import com.example.beacon_gps_locator.Models.LocInTime;
import com.example.beacon_gps_locator.Models.User;
import com.example.beacon_gps_locator.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ImageView img_back;
    private TextView tv_time_start, tv_time_end, tv_date, tv_day, tv_earned, tv_point, tv_distance, tv_time;

    private RelativeLayout rel_back, rel_update;
    private ArrayList<LocInTime> locInTimeList = new ArrayList<>();
    private float total_distance = 0;


    //firebaseStuff
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference locRef = FirebaseDatabase.getInstance().getReference().child("UsersLocation");
    String date = "";
    String time_start = "";
    String time_end = "";
    String day = "";
    DatabaseReference cycleRef = FirebaseDatabase.getInstance().getReference().child("CyclesHistory");

    String currentUser = "";

    AlertDialog.Builder builder;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        initFields();

        iniFirebaseFields();

        setListeners();

        retrieveInfo();

//        getLocationMarks();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_maps);
        mapFragment.getMapAsync(this);

    }

    private void setListeners() {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(53.32231, -6.25324), 15));

    }

    private void initFields() {

        Intent intent = getIntent();
        date = intent.getStringExtra("date");

        cycleRef = cycleRef.child(mAuth.getCurrentUser().getUid());

        tv_distance = findViewById(R.id.tv_distancea_map);
        tv_earned = findViewById(R.id.tv_earning_map);
        tv_time_start = findViewById(R.id.tv_start_map);
        tv_time_end = findViewById(R.id.tv_end_map);
        tv_earned = findViewById(R.id.tv_earning_map);
        tv_date = findViewById(R.id.tv_date_map);
        tv_day = findViewById(R.id.tv_day_map);
        tv_time = findViewById(R.id.tv_time);
        tv_point = findViewById(R.id.tv_points_map);
        img_back = findViewById(R.id.img_back_map);
        rel_back = findViewById(R.id.rel_back_map);
        rel_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        rel_update = findViewById(R.id.rel_update);
        rel_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLocationMarks();
            }
        });

        builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.progress);
        builder.setCancelable(false);
        dialog = builder.create();
        dialog.setCancelable(false);
    }

    private void iniFirebaseFields() {
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(this, LogOnActivity.class));
            finish();
        }
        currentUser = mAuth.getCurrentUser().getUid();
        locRef = locRef.child(currentUser).child(date);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BeaconReferenceApplication application = (BeaconReferenceApplication) getApplicationContext();
        application.disableMonitoring();
    }

    private void getLocationMarks() {
        total_distance = 0;
        locInTimeList.clear();
        Intent intent = getIntent();
        if (intent.getStringExtra("date") != null) {
            date = intent.getStringExtra("date");
            locRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        ArrayList<LatLng> list = new ArrayList<>();
                        boolean flag = false;
                        for (DataSnapshot ds : snapshot.getChildren()) {
                            LocInTime loc = ds.getValue(LocInTime.class);
                            if (loc.getTime() != null && loc.getTime().length() != 0){
                                flag = true;
                            }
                            locInTimeList.add(loc);
                            list.add(new LatLng(loc.getLatitude(), loc.getLongitude()));
                        }
                        if (flag == true){
                            dialog.show();
                        }
                        doCalculations();
                    }
                    else{
                        Toast.makeText(MapActivity.this, "No Activity found for this day", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } else {
            Toast.makeText(this, "You haven't selected a day", Toast.LENGTH_SHORT).show();
        }
    }

    private void doCalculations() {
        int start = 0;
        int end;

        for (int i = 0; i < locInTimeList.size(); i++) {
            LocInTime each = locInTimeList.get(i);
            if (each.getLatitude() == 0) {
                end = i;
                drawLines(start, end);
                calculateDistance(start, end);
                start = i + 1;
            }
        }
        end = locInTimeList.size() - 1;
        drawLines(start, end);
        calculateDistance(start, end);
        LocInTime first = locInTimeList.get(0);
        LocInTime last = locInTimeList.get(locInTimeList.size()-1);

        String time_start = first.getTime();
        String time_end = last.getTime();

        this.time_start = time_start;
        this.time_end = time_end;
        if (last.getDayOfWeek() != null){
            this.day = last.getDayOfWeek();
        }

        sendCalculationsToDB();

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(last.getLatitude(),last.getLongitude()), 15));

        MarkerOptions optionsStart = new MarkerOptions()
                .title("Start")
                .position(new LatLng(first.getLatitude(), first.getLongitude()))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mMap.addMarker(optionsStart);

        MarkerOptions optionsEnd = new MarkerOptions()
                .title("End")
                .position(new LatLng(last.getLatitude(), last.getLongitude()))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mMap.addMarker(optionsEnd);

        dialog.dismiss();

    }

    private void drawLines(int start, int end) {

        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
        for (int z = start; z <= end; z++) {
            LocInTime point = locInTimeList.get(z);
            double lat = point.getLatitude();
            double lng = point.getLongitude();
            if (lat != 0 && lng != 0) {
                options.add(new LatLng(lat, lng));
            }
        }
        Polyline line = mMap.addPolyline(options);

    }

    private void calculateDistance(int start, int end) {

        for (int i = start; i < end; i++) {
            LocInTime starter = locInTimeList.get(i);
            LocInTime ender = locInTimeList.get(i + 1);

            LatLng start_point = new LatLng(starter.getLatitude(), starter.getLongitude());
            LatLng end_point = new LatLng(ender.getLatitude(), ender.getLongitude());

            float[] distanceWidth = new float[2];
            Location.distanceBetween(
                    start_point.latitude,
                    start_point.longitude,
                    end_point.latitude,
                    end_point.longitude,
                    distanceWidth);

            float distance = distanceWidth[0];
            total_distance += distance;
        }

    }

    private void sendCalculationsToDB() {
        tv_date.setText(date);
        tv_time_end.setText(time_end);
        tv_time_start.setText(time_start);
        tv_distance.setText(total_distance + "");
        tv_day.setText(day);

        int hourStart = Integer.parseInt(time_start.substring(0, 2));
        int minStart = Integer.parseInt(time_start.substring(3, 5));

        int hourEnd = Integer.parseInt(time_end.substring(0, 2));
        int minEnd = Integer.parseInt(time_end.substring(3, 5));

        String time = "";

        if (minEnd == minStart && hourEnd == hourStart) {
            // pass
        } else {
            int hour = minStart < minEnd ? hourEnd - hourStart : hourEnd - 1 - hourStart;
            int min = minStart < minEnd ? minEnd - minStart : minEnd + 60 - minStart;
            time = hour + ":" + min;
        }

        tv_time.setText(time);

        HashMap<String, Object> map = new HashMap<>();
        map.put("distance", total_distance + "");
        map.put("time_start", time_start);
        map.put("time_end", time_end);
        map.put("date", date);
        map.put("day", day);
        cycleRef.child(date).updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.v("Tagger", "distance updated");
                } else {
                    Log.v("Tagger", "distance was not updated");
                }
            }
        });

    }

    private void retrieveInfo() {
        getLocationMarks();
    }

}