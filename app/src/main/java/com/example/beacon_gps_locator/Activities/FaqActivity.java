package com.example.beacon_gps_locator.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.beacon_gps_locator.R;

public class FaqActivity extends AppCompatActivity {


    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        initFields();

        setListeners();

    }

    private void initFields() {
        img_back = findViewById(R.id.img_back_faq);
    }

    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}