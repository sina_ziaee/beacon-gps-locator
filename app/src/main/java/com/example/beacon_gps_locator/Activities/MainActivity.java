package com.example.beacon_gps_locator.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.beacon_gps_locator.BackgroundWorks.BeaconReferenceApplication;
import com.example.beacon_gps_locator.BackgroundWorks.LocationService;
import com.example.beacon_gps_locator.Fragments.CampaignFragment;
import com.example.beacon_gps_locator.Fragments.HistoryFragment;
import com.example.beacon_gps_locator.Fragments.HomeFragment;
import com.example.beacon_gps_locator.Fragments.ProfileFragment;
import com.example.beacon_gps_locator.Models.LocInTime;
import com.example.beacon_gps_locator.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, BeaconConsumer {

    private BeaconManager beaconManager;
    private BottomNavigationView bottomNavigationView;
    private ImageView img_notifications;
    private boolean isUserSaidStart = false;
    public static boolean isGettingBeaconSignal = false;
    private boolean isGettingLocationSignal = false;
    public boolean isLookingForBeacons = false;

    int beaconCounter = 0;

    FloatingActionButton floatingActionButton;

    private int counter = 0;
//    private ImageView img_play;
    Button btn_tracking;

    public static boolean isRunning = false;

    //firebase stuff
    private DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference().child("UsersLocation");
    private DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users");
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseUser firebaseUser = mAuth.getCurrentUser();
    private String currentUser;

    LocationBroadcastReceiver locationReceiver;
    IntentFilter filter;
    Intent intent;

    DatabaseReference locRef = FirebaseDatabase.getInstance().getReference().child("UsersLocation");

    AlertDialog.Builder builder;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFields();

        initFirebaseUtils();

        if (savedInstanceState == null) {
            bottomNavigationView.setSelectedItemId(R.id.nav_home);
        }

        if (isNetworkConnected() == false){
            dialog.show();
        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void initFirebaseUtils() {
        if (firebaseUser == null) {
            gotoLogonActivity();
        }
        else {
            currentUser = firebaseUser.getUid();
            dbRef = dbRef.child(currentUser);
            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

            locRef = locRef.child(currentUser).child(date);
        }
    }

    private void initFields() {

        builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.internet_dialog);

        dialog = builder.create();

        locationReceiver = new LocationBroadcastReceiver();

        bottomNavigationView = findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        img_notifications = findViewById(R.id.img_notification);
        img_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(intent);
            }
        });
        btn_tracking = findViewById(R.id.btn_tracking);
        btn_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUserSaidStart == true) {
                    btn_tracking.setText("Start");
                    isUserSaidStart = false;
                    stopAll();
                } else {
                    gotoUtilsActivity();
                }
            }
        });
        floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoMapsActivity();
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.nav_home:
                fragment = HomeFragment.newInstance(getApplicationContext());
                break;
            case R.id.nav_history:
                fragment = HistoryFragment.newInstance(getApplicationContext());
                break;
            case R.id.nav_campaign:
                fragment = CampaignFragment.newInstance();
                break;
            case R.id.nav_profile:
                fragment = ProfileFragment.newInstance();
                break;
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (beaconManager != null && beaconManager.isBound(this)) {
            BeaconReferenceApplication application = (BeaconReferenceApplication) getApplicationContext();
            application.disableMonitoring();
            beaconManager.unbind(this);
        }
        if (isGettingLocationSignal){
            isGettingLocationSignal = false;
            getApplicationContext().unregisterReceiver(locationReceiver);
            boolean temp = stopService(intent);
            LocationService.stopLocationService();
            Log.v("Tagger", "Trying to stop location service: " + temp);

        }

        BeaconReferenceApplication application = (BeaconReferenceApplication) getApplicationContext();
        application.disableMonitoring();

    }

    @Override
    public void onBeaconServiceConnect() {
        RangeNotifier rangeNotifier = new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                boolean flag = false;
                isLookingForBeacons = true;
                if (beacons.size() > 0) {
                    for (Beacon beacon : beacons) {
                        if (beacon.getId1().toString().equals(UtilsActivity.beacon_id1)) {
                            flag = true;
                        }
                    }
                    if (flag == true) {
                        if (isUserSaidStart == false) {
                            beaconManager.unbind(MainActivity.this);
                        }
                        else {
                            isGettingBeaconSignal = true;
                            logger("Found your beacon");
                        }
                    } else {
                        logger("Found beacon not yours");
                        isGettingBeaconSignal = false;
                    }

                } else {
                    isGettingBeaconSignal = false;
                    logger("Can't find any beacon");
                }
            }

        };
        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
            beaconManager.addRangeNotifier(rangeNotifier);
        } catch (RemoteException e) {
        }
    }

    private void gotoUtilsActivity() {
        Intent intent = new Intent(this, UtilsActivity.class);
        startActivityForResult(intent, 123);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123) {
            if (resultCode == RESULT_OK) {
                boolean result = data.getBooleanExtra("is_selected_beacon", false);
                if (result == true) {
                    beaconCounter++;
                    isLookingForBeacons = true;
                    btn_tracking.setText("Stop");
                    if (beaconCounter == 1) {
                        beaconManager = BeaconManager.getInstanceForApplication(this);
                        beaconManager.bind(this);
                    }
                    else {
                        beaconManager.bind(this);
                    }
                    isUserSaidStart = true;
                    logger("started location");
                    startLocationService();
                } else {
                    btn_tracking.setText("Start");
                    isUserSaidStart = false;
                }
            } else {
                btn_tracking.setText("Start");
                isUserSaidStart = false;
            }
        }
    }

    public void logger(String line) {
        Log.v("Tagger", line);
    }

    public class LocationBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("ACT_LOC")) {

                isGettingLocationSignal = true;
                isRunning = true;

                String time = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                Calendar sCalendar = Calendar.getInstance();
                String dayLongName = sCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());

                double lat = intent.getDoubleExtra("latitude", 0f);
                double lng = intent.getDoubleExtra("longitude", 0f);
                Toast.makeText(context, "Latitude is: " + lat + " , Longitude is: " + lng, Toast.LENGTH_SHORT).show();
//                logger("Latitude is: " + lat + " , Longitude is: " + lng);

                if (isGettingBeaconSignal == true) {
//                    logger("getting signal");
                    saveDataInDatabase(date, time, lat, lng, dayLongName);
                    counter = 0;
                } else {
                    counter++;
//                    logger("not getting signal");
                    if (counter == 1) {
                        saveDataInDatabase(date, time, 0, 0, dayLongName);
                    }
                }

            }
        }
    }

    private void startLocationService() {
        if (isGettingLocationSignal == false) {
            filter = new IntentFilter("ACT_LOC");
            getApplicationContext().registerReceiver(locationReceiver, filter);
            isGettingLocationSignal = true;
            intent = new Intent(MainActivity.this, LocationService.class);
            startService(intent);
        }
    }

    private void saveDataInDatabase(String date, final String time, double lat, double lng, String dayLongName) {

        LocInTime locInTime = new LocInTime(lng, lat, time, date, dayLongName);
        dbRef.child(date).child(time).setValue(locInTime).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.v("Tagger", "In save DataInDatabase: Info sent to sever at : " + time);
                } else {
                    Log.v("Tagger", "Error: " + task.getException().toString());
                    Toast.makeText(MainActivity.this, "Info was not sent to server", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void gotoLogonActivity() {
        Intent intent = new Intent(this, LogOnActivity.class);
        startActivity(intent);

        finish();
    }

    private void gotoMapsActivity() {
//        stopLocationService();
        Intent intent = new Intent(this, MapActivity.class);
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        intent.putExtra("date", date);
        startActivity(intent);
    }

    public void stopAll() {
        isGettingBeaconSignal = false;
        if (beaconManager != null && beaconManager.isBound(this)) {
            beaconManager.unbind(this);
            Log.v("Tagger", "Unbounded beacon manager");
        }

        BeaconReferenceApplication application = (BeaconReferenceApplication) getApplicationContext();
        application.disableMonitoring();

        // stop location receiver
        if (isGettingLocationSignal){
            isLookingForBeacons = false;
            isGettingLocationSignal = false;
            getApplicationContext().unregisterReceiver(locationReceiver);
            boolean temp = stopService(intent);
            LocationService.stopLocationService();
            Log.v("Tagger", "Trying to stop location service: " + temp);

        }

    }

}
