package com.example.beacon_gps_locator.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.beacon_gps_locator.R;

public class PrivacyPolicyActivity extends AppCompatActivity {

    private ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        initFields();

        setListeners();

    }

    private void initFields() {
        img_back = findViewById(R.id.img_back_privacy_policy);
    }

    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}