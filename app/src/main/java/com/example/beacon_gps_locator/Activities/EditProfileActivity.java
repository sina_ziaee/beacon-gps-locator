package com.example.beacon_gps_locator.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.beacon_gps_locator.Models.User;
import com.example.beacon_gps_locator.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity {

    ImageView img_back;
    EditText et_fullname, et_email, et_country, et_full_address, et_bike_brand, et_bike_type;
    CircleImageView img_profile_pic, img_choose;
    Button btn_save_changes, btn_cancel;

    //firebase stuff
    DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference().child("Users");

    private String currentUser = "";
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private String downloadImageUrl = null;
    private Uri imageUri;

    AlertDialog.Builder builder;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        initFields();

        setListeners();

        retrieveInfoFromDB();

    }

    private void initFields() {
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(this, LogOnActivity.class));
            finish();
        }
        currentUser = mAuth.getCurrentUser().getUid();
        usersRef = usersRef.child(currentUser);

        img_back = findViewById(R.id.img_back_edit_profile);
        img_profile_pic = findViewById(R.id.img_edit_profile);
        img_choose = findViewById(R.id.img_choose);
        et_bike_brand = findViewById(R.id.et_bike_brand);
        et_bike_type = findViewById(R.id.et_bike_type);
        et_full_address = findViewById(R.id.et_full_address);
        et_fullname = findViewById(R.id.et_fullname_edit_profile);
        et_country = findViewById(R.id.et_country);
        et_email = findViewById(R.id.et_email_edit_profile);
        btn_save_changes = findViewById(R.id.btn_save_changes);
        btn_cancel = findViewById(R.id.btn_cancel);

        builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.progress);
        builder.setCancelable(false);
        dialog = builder.create();
        dialog.setCancelable(false);

    }


    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1)
                        .start(EditProfileActivity.this);
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(EditProfileActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        btn_save_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveInformationOnDB();
            }
        });

    }

    private void saveInformationOnDB() {
        if (validateInputs() == true){
            dialog.show();
            if (imageUri == null){
                updateProfile();
            }
            else{
                uploadImage();
            }
        }
    }

    private void uploadImage() {
        final StorageReference imgRef = FirebaseStorage.getInstance().getReference("ProfileImages")
                .child(mAuth.getCurrentUser().getUid());
        UploadTask uploadTask = imgRef.putFile(imageUri);

        uploadTask.continueWithTask(new Continuation() {
            @Override
            public Object then(@NonNull Task task) throws Exception {
                if (!task.isSuccessful()) {
                    Log.v("error", task.getException() + "");
                    throw task.getException();
                }
                return imgRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    downloadImageUrl = task.getResult().toString();
                    updateProfile();
                } else {
                    Log.v("Tagger", task.getException().toString());
                    Toast.makeText(EditProfileActivity.this, "Image was not uploaded", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void updateProfile() {

        String fullName = et_fullname.getText().toString();

        String email = et_email.getText().toString();

        String country = et_country.getText().toString();

        String full_address = et_full_address.getText().toString();

        String bike_brand = et_bike_brand.getText().toString();

        String bike_type = et_bike_type.getText().toString();

        HashMap<String, Object> map = new HashMap<>();

        map.put("fullName", fullName);
        map.put("email", email);
        map.put("country", country);
        map.put("fullAddress", full_address);
        map.put("bikeBrand", bike_brand);
        map.put("bikeType", bike_type);

        // user doesn't want to upload any picture
        if (downloadImageUrl == null){
            // pass
        }
        // user has uploaded his/her image
        else {
            map.put("imgUrl", downloadImageUrl);
        }

        usersRef.updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                dialog.dismiss();
                if (task.isSuccessful()){
                    Toast.makeText(EditProfileActivity.this, "Information updated Successfully", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(EditProfileActivity.this, "FailedToUpdateInformation", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean validateInputs(){
        String fullName = et_fullname.getText().toString();
        if (fullName.length() == 0){
            Toast.makeText(this, "Please edit full name", Toast.LENGTH_SHORT).show();
            return false;
        }
        String email = et_email.getText().toString();
        if (email.length() == 0){
            Toast.makeText(this, "Please edit email", Toast.LENGTH_SHORT).show();
            return false;
        }
        String country = et_country.getText().toString();
        if (country.length() == 0){
            Toast.makeText(this, "Please edit country", Toast.LENGTH_SHORT).show();
            return false;
        }
        String full_address = et_full_address.getText().toString();
        if (full_address.length() == 0){
            Toast.makeText(this, "Please edit ful address", Toast.LENGTH_SHORT).show();
            return false;
        }
        String bike_brand = et_bike_brand.getText().toString();
        if (bike_brand.length() == 0){
            Toast.makeText(this, "Please edit bike brand", Toast.LENGTH_SHORT).show();
            return false;
        }
        String bike_type = et_bike_type.getText().toString();
        if (bike_type.length() == 0){
            Toast.makeText(this, "Please edit bike type", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void compressImage(){

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                imageUri = resultUri;
                Log.v("Tagger", resultUri.toString());
                img_profile_pic.setImageURI(imageUri);
            }
            else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Exception error = result.getError();
                Log.v("Tagger", error.toString());
                Toast.makeText(this, "Failed to crop", Toast.LENGTH_SHORT).show();
            }

            else {
                Toast.makeText(this, "Nothing happened", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(this, requestCode + "", Toast.LENGTH_SHORT).show();
        }
    }

    private void retrieveInfoFromDB(){
        dialog.show();
        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    User user = snapshot.getValue(User.class);
                    et_fullname.setText(user.getFullName());
                    et_email.setText(user.getEmail());
                    et_bike_brand.setText(user.getBikeBrand());
                    et_bike_type.setText(user.getBikeType());
                    et_country.setText(user.getCountry());
                    et_full_address.setText(user.getFullAddress());

                    if (user.getImgUrl().length() != 0) {
                        Picasso.get().load(user.getImgUrl()).into(img_profile_pic);
                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                dialog.dismiss();
            }
        });
    }
}