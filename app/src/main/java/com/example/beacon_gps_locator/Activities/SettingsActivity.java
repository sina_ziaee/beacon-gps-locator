package com.example.beacon_gps_locator.Activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.beacon_gps_locator.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.auth.FirebaseAuth;

public class SettingsActivity extends AppCompatActivity {

    ImageView img_back;
    LinearLayout rel_logout, rel_faq, rel_user_agreement, rel_privacy_policy;

    //firebase stuff
    FirebaseAuth mAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initFields();

        setListeners();

    }

    private void initFields() {
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(this, LogOnActivity.class));
            finish();
        }
        img_back = findViewById(R.id.img_back_settings);
        rel_faq = findViewById(R.id.lin_faq);
        rel_logout = findViewById(R.id.rel_logout);
        rel_privacy_policy = findViewById(R.id.lin_privacy_policy);
        rel_user_agreement = findViewById(R.id.lin_user_agreement);
    }

    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        rel_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoFaqActivity();
            }
        });

        rel_user_agreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        rel_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        rel_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                logout();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                builder.setMessage("Do you really want to Sign out ?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });
    }

    private void logout() {
        mAuth.signOut();
        startActivity(new Intent(SettingsActivity.this, LogOnActivity.class));
        finish();
    }

    private void gotoFaqActivity(){
        startActivity(new Intent(this, FaqActivity.class));
    }

}