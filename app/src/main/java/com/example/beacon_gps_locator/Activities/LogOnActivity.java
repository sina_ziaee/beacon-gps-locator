package com.example.beacon_gps_locator.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beacon_gps_locator.Models.User;
import com.example.beacon_gps_locator.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.TimeUnit;

public class LogOnActivity extends AppCompatActivity {

    //    private CountryCodePicker ccp;
    private EditText et_phone, et_name, et_code;
    private Button btn_send;
    private TextView tv_goto_login;
    private boolean inVerificationMode = false;

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    AlertDialog.Builder builder;
    Dialog dialog;

    //firebase stuff
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallBacks;
    private DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users");
    private String currentUser;

    AlertDialog.Builder builder2;
    Dialog dialog2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logon);

        initFields();

        initFirebaseUtils();

        setListeners();

        while (isNetworkConnected() == false){
            dialog2.show();
        }

    }

    private void initFields() {
        builder2 = new AlertDialog.Builder(this);
        builder2.setView(R.layout.internet_dialog);
        dialog2 = builder2.create();

        tv_goto_login = findViewById(R.id.tv_logon_goto_login);
        et_name = findViewById(R.id.et_logon_username);
        et_phone = findViewById(R.id.et_logon_phone);
        btn_send = findViewById(R.id.btn_logon);
        et_code = findViewById(R.id.et_verification_field_logon);

        builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.progress);
        builder.setCancelable(false);
        dialog = builder.create();
        dialog.setCancelable(false);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void setListeners() {
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inVerificationMode == false) {
                    checkIfPersonExists();
                } else {
                    String verificationCode = et_code.getText().toString();
                    if (verificationCode.length() == 0) {
                        Toast.makeText(LogOnActivity.this, "Enter verification code", Toast.LENGTH_SHORT).show();
                    } else {
                        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, verificationCode);
                        signInWithPhoneAuthCredential(credential);
                    }
                }
            }
        });

        tv_goto_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoLoginActivity();
            }
        });
    }

    private void checkIfPersonExists() {
        if (checkValidInput()) {
            dialog.show();
            final String phone = et_phone.getText().toString();
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    boolean flag = false;
                    Log.v("Tagger", "here");
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        User user = ds.getValue(User.class);
                        Log.v("Tagger", user.getPhone());
                        if (user.getPhone().equals(phone)) {
                            flag = true;
                        }
                    }
                    if (flag == false) {
                        on_btn_send_clicked();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(LogOnActivity.this, "A user with this phone exists", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        gotoLoginActivity();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        } else {
            Toast.makeText(LogOnActivity.this, "Enter correct inputs", Toast.LENGTH_SHORT).show();
        }

    }

    private void on_btn_send_clicked() {
        String phone = et_phone.getText().toString();

        if (phone.startsWith("00")) {
            phone = "+" + phone.substring(2);
        }
        else if (phone.startsWith("+")) {
        }
        else {
            phone = "+" + phone;
        }
        Toast.makeText(LogOnActivity.this, "A code is gonna be sent to your phone", Toast.LENGTH_SHORT).show();
        sendVerificationCode(phone);

        btn_send.setText("Verify");
        et_code.setVisibility(View.VISIBLE);
        inVerificationMode = true;

    }

    private boolean checkValidInput() {
        String fullName = et_name.getText().toString();
        String phone = et_phone.getText().toString();
        if (fullName.length() == 0) {
            return false;
        }
        if (phone.length() == 0) {
            return false;
        }
        return true;
    }

    private void initFirebaseUtils() {

        mCallBacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                dialog.dismiss();
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                dialog.dismiss();
                Toast.makeText(LogOnActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                inVerificationMode = false;
                btn_send.setText("Log on");
                Log.v("Tagger", e.getMessage());
                Toast.makeText(LogOnActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();

            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);

                dialog.dismiss();

                mVerificationId = s;
                mResendToken = forceResendingToken;
                Toast.makeText(LogOnActivity.this, "A code is gonna be sent to your phone in at most 30 second, wait please", Toast.LENGTH_LONG).show();

            }
        };
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        dialog.show();
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(LogOnActivity.this, "Congratulations you are logged in successfully", Toast.LENGTH_SHORT).show();
                            savingUserInformationOnDatabase();
                        } else {
                            String e = task.getException().toString();
                            Toast.makeText(LogOnActivity.this, "Error: " + e, Toast.LENGTH_SHORT).show();
                            Log.v("Tagger", e);
                        }
                    }
                });

    }

    private void savingUserInformationOnDatabase() {
        String fullname = et_name.getText().toString();
        String phone = et_phone.getText().toString();

        currentUser = mAuth.getCurrentUser().getUid();

        User user = new User(fullname, "", "", "", "", "", phone, "", "");

        userRef.child(currentUser).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                dialog.dismiss();
                if (task.isSuccessful()) {
                    Toast.makeText(LogOnActivity.this, "Your information was saved in database successfully", Toast.LENGTH_SHORT).show();
                    gotoMainActivity();
                } else {
                    String exception = task.getException().toString();
                    Log.v("Tagger", exception);
                    Toast.makeText(LogOnActivity.this, "Your info was not saved in database", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void sendVerificationCode(String phone) {
        Log.v("Tagger", phone);
        if (!phone.equals("")) {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phone,
                    120,
                    TimeUnit.SECONDS,
                    LogOnActivity.this,
                    mCallBacks
            );
        } else {
            Toast.makeText(LogOnActivity.this, "Please write valid phone number.", Toast.LENGTH_SHORT).show();
        }

    }

    private void gotoLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}