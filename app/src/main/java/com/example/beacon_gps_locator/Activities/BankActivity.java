package com.example.beacon_gps_locator.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.beacon_gps_locator.Models.BankInfo;
import com.example.beacon_gps_locator.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;

public class BankActivity extends AppCompatActivity {

    ImageView img_back;
    EditText et_bank_name, et_iban_number, et_account_name;
    Button btn_save_changes;
    RelativeLayout rel_upload;
    ImageView img_id_card;

    // firebase stuff
    DatabaseReference bankRef = FirebaseDatabase.getInstance().getReference().child("BankInfo");
    private String currentUser = "";
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private String downloadImageUrl = null;
    private Uri imageUri;

    AlertDialog.Builder builder;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank);

        initFields();

        setListeners();

        retrieveInfoFromDB();

    }

    private void initFields() {
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(this, LogOnActivity.class));
            finish();
        }
        currentUser = mAuth.getCurrentUser().getUid();
        bankRef = bankRef.child(currentUser);

        img_back = findViewById(R.id.img_back_bank);
        et_bank_name = findViewById(R.id.et_bank_name);
        et_iban_number = findViewById(R.id.et_IBAN_Number);
        et_account_name = findViewById(R.id.et_account_name);
        btn_save_changes = findViewById(R.id.btn_save_changes_bank);
        rel_upload = findViewById(R.id.rel_upload_id_card);
        img_id_card = findViewById(R.id.img_upload_photo);

        builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.progress);
        builder.setCancelable(false);
        dialog = builder.create();
        dialog.setCancelable(false);

    }

    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        rel_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 2)
                        .start(BankActivity.this);
            }
        });

        btn_save_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveInfoOnDB();
            }
        });
    }

    private void uploadImage(){
        final StorageReference imgRef = FirebaseStorage.getInstance().getReference("Profile_Images")
                .child(mAuth.getCurrentUser().getUid());
        UploadTask uploadTask = imgRef.putFile(imageUri);

        uploadTask.continueWithTask(new Continuation() {
            @Override
            public Object then(@NonNull Task task) throws Exception {
                if (!task.isSuccessful()) {
                    Log.v("error", task.getException() + "");
                    throw task.getException();
                }
                return imgRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    downloadImageUrl = task.getResult().toString();
                    updateBankInfo();
                } else {
                    Log.v("Tagger", task.getException().toString());
                    Toast.makeText(BankActivity.this, "Image was not uploaded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean validateInputs(){
        String bankName = et_bank_name.getText().toString();
        String bankIBANNumber = et_iban_number.getText().toString();
        String accountName = et_account_name.getText().toString();

        if(bankName.length() == 0){
            Toast.makeText(this, "Please fill bank name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (bankIBANNumber.length() == 0){
            Toast.makeText(this, "Please fill IBAN number", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (accountName.length() == 0){
            Toast.makeText(this, "Please fill account name", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }

    private void saveInfoOnDB() {
        if (validateInputs() == true){
            dialog.show();
            if (imageUri == null){
                updateBankInfo();
            }
            else{
                uploadImage();
            }
        }
    }

    private void updateBankInfo() {
        String bankName = et_bank_name.getText().toString();
        String bankIBANNumber = et_iban_number.getText().toString();
        String accountName = et_account_name.getText().toString();

        HashMap<String, Object> map = new HashMap<>();
        map.put("bankName", bankName);
        map.put("bankIBANNumber", bankIBANNumber);
        map.put("accountName", accountName);

        if (downloadImageUrl == null){
            // pass
        }
        // user has uploaded his/her image
        else {
            map.put("imgUrl", downloadImageUrl);
        }

        bankRef.updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                dialog.dismiss();
                if (task.isSuccessful()){
                    Toast.makeText(BankActivity.this, "Information updated Successfully", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(BankActivity.this, "Failed to update bank info", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                imageUri = resultUri;
                Log.v("Tagger", resultUri.toString());
                img_id_card.setImageURI(imageUri);
            }
            else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Exception error = result.getError();
                Log.v("Tagger", error.toString());
                Toast.makeText(this, "Failed to crop", Toast.LENGTH_SHORT).show();
            }

            else {
                Toast.makeText(this, "Nothing happened", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(this, requestCode + "", Toast.LENGTH_SHORT).show();
        }
    }

    private void retrieveInfoFromDB(){
        dialog.show();
        bankRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    BankInfo bankInfo = snapshot.getValue(BankInfo.class);
                    et_account_name.setText(bankInfo.getBankName());
                    et_bank_name.setText(bankInfo.getBankName());
                    et_iban_number.setText(bankInfo.getBankIBANNumber());
                    if (bankInfo.getImgUrl() != null){
                        Picasso.get().load(bankInfo.getImgUrl()).into(img_id_card);
                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                dialog.dismiss();
            }
        });
    }

}