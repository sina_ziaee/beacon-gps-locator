package com.example.beacon_gps_locator.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beacon_gps_locator.Models.User;
import com.example.beacon_gps_locator.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.TimeUnit;

public class LoginActivity extends AppCompatActivity {

    private EditText et_code, et_phone;
    private Button btn_send;
    private TextView tv_goto_logon;
    private boolean inVerificationMode = false;

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    //firebase stuff
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallBacks;
    DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users");

    AlertDialog.Builder builder;
    Dialog dialog;

    AlertDialog.Builder builder2;
    Dialog dialog2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initFields();
        setListeners();
        initFirebaseUtils();

        while (isNetworkConnected() == false){
            dialog2.show();
        }

    }

    private void setListeners() {
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inVerificationMode == false){
                    checkIfPersonExists();
                }
                else {
                    String verificationCode = et_code.getText().toString();
                    if (verificationCode.length() == 0) {
                        Toast.makeText(LoginActivity.this, "Enter verification code", Toast.LENGTH_SHORT).show();
                    } else {
                        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, verificationCode);
                        signInWithPhoneAuthCredential(credential);
                    }
                }

            }
        });
        tv_goto_logon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoLogonActivity();
            }
        });
    }

    private void initFields() {

        builder2 = new AlertDialog.Builder(this);
        builder2.setView(R.layout.internet_dialog);
        dialog2 = builder2.create();

        et_code = findViewById(R.id.et_verification_field_login);
        et_phone = findViewById(R.id.et_login_phone);
        btn_send = findViewById(R.id.btn_login);
        tv_goto_logon = findViewById(R.id.tv_login_goto_logon);

        builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.progress);
        builder.setCancelable(false);
        dialog = builder.create();
        dialog.setCancelable(false);

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void gotoLogonActivity(){
        Intent intent = new Intent(this, LogOnActivity.class);
        startActivity(intent);
        finish();
    }

    private void checkIfPersonExists(){
        if (checkValidInput()) {
            dialog.show();
            final String phone = et_phone.getText().toString();
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    boolean flag = false;
                    Log.v("Tagger", "here");
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        User user = ds.getValue(User.class);
                        Log.v("Tagger", user.getPhone());
                        if (user.getPhone().equals(phone)) {
                            flag = true;
                        }
                    }
                    if (flag == true) {
                        on_btn_send_clicked();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(LoginActivity.this, "This phone does not exists", Toast.LENGTH_SHORT).show();
                        gotoLogonActivity();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    private void initFirebaseUtils() {

        mCallBacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                dialog.show();
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                dialog.dismiss();
                inVerificationMode = false;
                btn_send.setText("Log in");
                Toast.makeText(LoginActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                dialog.dismiss();
                mVerificationId = s;
                mResendToken = forceResendingToken;
                Toast.makeText(LoginActivity.this, "A code has been sent to your phone, please check it", Toast.LENGTH_LONG).show();

            }
        };
    }

    private void on_btn_send_clicked() {
        String phone = et_phone.getText().toString();

        if (inVerificationMode == false){
                if (phone.startsWith("00")){
                    phone = "+" + phone.substring(2);
                }
                else if (phone.startsWith("+")){

                }
                else{
                    phone = "+" + phone;
                }
                Toast.makeText(LoginActivity.this, "A code is gonna be sent to your phone in at most 30 second, wait please", Toast.LENGTH_LONG).show();
                sendVerificationCode(phone);

                btn_send.setText("Verify");
                et_code.setVisibility(View.VISIBLE);
                inVerificationMode = true;

        }
        else {// in verification mode
            String verificationCode = et_code.getText().toString();
            if (verificationCode.length()==0){
                Toast.makeText(LoginActivity.this, "Enter verification code", Toast.LENGTH_SHORT).show();
            }
            else {
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, verificationCode);
                signInWithPhoneAuthCredential(credential);
            }
        }
    }

    private boolean checkValidInput(){
        String phone = et_phone.getText().toString();
        if (phone.length() == 0){
            Toast.makeText(LoginActivity.this, "Enter correct inputs", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential){
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        dialog.dismiss();
                        if (task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Congratulations you are logged in successfully", Toast.LENGTH_SHORT).show();
                            gotoMainActivity();
                        }
                        else {
                            String e = task.getException().toString();
                            Toast.makeText(LoginActivity.this, "Error: " + e, Toast.LENGTH_SHORT).show();
                            Log.v("Tagger", e);
                        }
                    }
                });
    }

    private void gotoMainActivity() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void sendVerificationCode(String phone){
        Log.v("Tagger", phone);
        if (!phone.equals("")){
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phone,
                    120,
                    TimeUnit.SECONDS,
                    LoginActivity.this,
                    mCallBacks
            );
        }
        else {
            Toast.makeText(LoginActivity.this, "Please write valid phone number.", Toast.LENGTH_SHORT).show();
        }

    }

}