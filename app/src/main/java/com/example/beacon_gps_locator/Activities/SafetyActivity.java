package com.example.beacon_gps_locator.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.beacon_gps_locator.R;

public class SafetyActivity extends AppCompatActivity {

    ImageView img_back;
    RelativeLayout rel_cycle_safety, rel_quick_tips, rel_rules_of_road;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safety);

        initFields();

        setListeners();

    }

    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        rel_rules_of_road.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        rel_quick_tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        rel_cycle_safety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void initFields() {
        img_back = findViewById(R.id.img_back_safety);

        rel_cycle_safety = findViewById(R.id.rel_cycling_in_dublin);
        rel_quick_tips = findViewById(R.id.rel_quick_tips);
        rel_rules_of_road = findViewById(R.id.rel_rules_of_the_road);
    }
}