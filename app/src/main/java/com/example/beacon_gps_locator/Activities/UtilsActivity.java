package com.example.beacon_gps_locator.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beacon_gps_locator.Adapters.BeaconsAdapter;
import com.example.beacon_gps_locator.BackgroundWorks.BeaconReferenceApplication;
import com.example.beacon_gps_locator.Models.CycleSummary;
import com.example.beacon_gps_locator.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class UtilsActivity extends AppCompatActivity implements BeaconConsumer, EasyPermissions.PermissionCallbacks {

    String TAG = "Tagger";
    boolean checkPermissions = false;
    boolean checkBluetoothIsEnabled = false;
    boolean checkLocationIsEnabled = false;
    private BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
    RecyclerView recyclerView;
    ImageView img_back;
    TextView tv_beacon;

    AlertDialog.Builder builder;
    Dialog dialog;

    public static String beacon_id1;

    // firebase stuff
    DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference().child("CyclesHistory");
    FirebaseAuth mAuth = FirebaseAuth.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utils);

        initFields();

        if (checkPermissions == false) {
            requestPermissions();
        }

        if (checkBluetoothIsEnabled == false) {
            enableBluetooth();
        }

        if (checkLocationIsEnabled == false) {
            enableLocation();
        }

        if (isNetworkConnected() == false) {
            dialog.show();
        }

        startMonitoringLocations();

    }

    private void initFields() {
        builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.internet_dialog);
        dialog = builder.create();

        tv_beacon = findViewById(R.id.tv_beacon_util);
        recyclerView = findViewById(R.id.rec_beacons_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        img_back = findViewById(R.id.img_utils_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMainActivity();
            }
        });

        if(mAuth.getCurrentUser() != null){
            String currentUser = mAuth.getCurrentUser().getUid();
            dbRef = dbRef.child(currentUser);
        }
        else {
            startActivity(new Intent(this, LogOnActivity.class));
            finish();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!beaconManager.isBound(UtilsActivity.this)) {
            beaconManager.bind(UtilsActivity.this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(this);
        BeaconReferenceApplication application = (BeaconReferenceApplication) getApplicationContext();
        application.disableMonitoring();
        beaconManager = null;
    }

    public void enableLocation() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked

                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                // pass
                            } else {
                                checkLocationIsEnabled = true;
                            }

                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(UtilsActivity.this);
            builder.setMessage("In order to save your location the app requires location access, So we are going to redirect you to your settings to enable it").setPositiveButton("Ok", dialogClickListener)
                    .show();

        } else {
            checkLocationIsEnabled = true;
        }
    }

    private void enableBluetooth() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
            checkBluetoothIsEnabled = true;
        } else {
            mBluetoothAdapter.enable();
            checkBluetoothIsEnabled = true;
        }
    }

    @AfterPermissionGranted(123)
    private void requestPermissions() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN};
        if (EasyPermissions.hasPermissions(this, perms)) {
//            Toast.makeText(this, "You have all permissions", Toast.LENGTH_SHORT).show();
            checkPermissions = true;
            return;
        } else {
            EasyPermissions.requestPermissions(this, "In terms of using this application you need to grant Location and bluetooth permission",
                    123, perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        checkPermissions = true;
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onBeaconServiceConnect() {
        RangeNotifier rangeNotifier = new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                if (beacons.size() > 0) {
                    showAllBeacons(beacons);
                    tv_beacon.setVisibility(View.INVISIBLE);
                } else {
//                    Toast.makeText(UtilsActivity.this, "No beacons Around", Toast.LENGTH_SHORT).show();
                    logger("No beacon around");
                }
            }

        };
        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
            beaconManager.addRangeNotifier(rangeNotifier);
//            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
//            beaconManager.addRangeNotifier(rangeNotifier);
        } catch (RemoteException e) {
        }
    }

    private void showAllBeacons(Collection<Beacon> beacons) {
        ArrayList<Beacon> list = new ArrayList<>();
        list.addAll(beacons);

        BeaconsAdapter adapter = new BeaconsAdapter(list);
        recyclerView.setAdapter(adapter);
    }

    private void logger(String line) {
        Log.v(TAG, line);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        gotoMainActivity();
    }

    private void gotoMainActivity() {
        Intent intent = new Intent();
        intent.putExtra("is_selected_beacon", false);
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }

    private void startMonitoringLocations() {
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        Calendar calendar = Calendar.getInstance();
        String day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(calendar.getTime());
        CycleSummary cycleSummary = new CycleSummary(date, "", "", "", "", "", day);
        dbRef.child(date).setValue(cycleSummary).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    logger("added new date on db");
                } else {
                    logger("failed to add new date on db");
                }
            }
        });
    }

}