package com.example.beacon_gps_locator.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.beacon_gps_locator.Adapters.MessageAdapter;
import com.example.beacon_gps_locator.Models.Message;
import com.example.beacon_gps_locator.R;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CustomerSupportActivity extends AppCompatActivity {

    private ImageView img_back, img_send;
    private EditText et_message_field;
    private RecyclerView recyclerView;

    //firebase stuff
    DatabaseReference msgRef = FirebaseDatabase.getInstance().getReference().child("messages");
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    String currentUser;
    MessageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_support);

        initFields();

        initFirebase();

        setListeners();

        setRecyclerView();

    }

    private void initFirebase() {
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(this, LogOnActivity.class));
            finish();
        }
        currentUser = mAuth.getCurrentUser().getUid();
        msgRef = msgRef.child(currentUser);
    }

    private void initFields() {
        img_back = findViewById(R.id.img_back_customer_support);
        img_send = findViewById(R.id.img_send);
        et_message_field = findViewById(R.id.et_message_field);
        recyclerView = findViewById(R.id.rec_chat);
    }

    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendingMessage();
            }
        });
    }


    private void setRecyclerView() {
        FirebaseRecyclerOptions<Message> options = new FirebaseRecyclerOptions.Builder<Message>()
                .setQuery(msgRef, Message.class)
                .build();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MessageAdapter(options);
        recyclerView.setAdapter(adapter);
        // To scroll bottom
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            public void onItemRangeInserted(int positionStart, int itemCount) {
                recyclerView.smoothScrollToPosition(adapter.getItemCount());
            }
        });
    }


    private void sendingMessage() {
        String messageBody = et_message_field.getText().toString();
        String time = getTime();
        String date = getDate();
        String sender = currentUser;
        String receiver = "Company";
        Message message = new Message(messageBody, sender, receiver, date, time);

        et_message_field.setText("");

        msgRef.push().setValue(message).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(CustomerSupportActivity.this, "Message send", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(CustomerSupportActivity.this, "Failed to send the message", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.startListening();
    }

    @Override
    protected void onPause() {
        super.onPause();
        adapter.stopListening();
    }

    private String getTime(){
        String time = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        return time;
    }
    private String getDate(){
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        return date;
    }

}